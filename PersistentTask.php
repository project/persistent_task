<?php

/**
 * @file
 * PersistentTask class
 */

/**
 * Main class to interact with tasks.
 */
class PersistentTask {
  const STATUS_NEW = 'new';
  const STATUS_IN_PROGRESS = 'in_progress';
  const STATUS_COMPLETED = 'competed';
  const STATUS_FAILED = 'failed';

  /**
   * Get allowed task statuses.
   *
   * @param bool|FALSE $onlyFinal
   *
   * @return array
   */
  public static function getStatuses($onlyFinal = FALSE) {
    if ($onlyFinal) {
      return [
        self::STATUS_COMPLETED => t('Completed'),
        self::STATUS_FAILED => t('Failed'),
      ];
    }

    return [
      self::STATUS_NEW => t('New'),
      self::STATUS_IN_PROGRESS => t('In progress'),
      self::STATUS_COMPLETED => t('Completed'),
      self::STATUS_FAILED => t('Failed'),
    ];
  }

  /**
   * Create new task.
   *
   * @param $type
   * @param array $data
   * @return array|object
   */
  public static function create($type, array $data = []) {
    $task = array(
      'created' => time(),
      'status' => self::STATUS_NEW,
      'type' => $type,
      'data' => $data,
    );
    $task = (object) $task;
    drupal_write_record('persistent_task', $task);

    return $task;
  }

  /**
   * Load task by ID.
   *
   * @param $tid
   *
   * @return mixed
   */
  public static function load($tid) {
    $query = db_select('persistent_task', 't');
    $query->fields('t');
    $query->condition('tid', $tid);

    $task = $query->execute()->fetch();
    $task->data = unserialize($task->data);

    return $task;
  }

  public static function update($task) {
    return drupal_write_record('persistent_task', $task, 'tid');
  }

  /**
   * Update task status.
   *
   * @param $task
   * @param $status
   *
   * @return bool
   */
  public static function updateStatus($task, $status) {
    if (empty($status) || !in_array($status, array_keys(self::getStatuses()))) {
      return FALSE;
    }

    if (in_array($status, array_keys(self::getStatuses(TRUE)))) {
      $task->completed = time();
      $task->started = $task->started ? $task->started : $task->completed;
    }
    elseif ($status != self::STATUS_NEW) {
      $task->started = time();
    }

    $task->status = $status;

    return self::update($task);
  }

  /**
   * Delete task by ID.
   *
   * @param $tid
   */
  public static function delete($tid) {
    db_delete('persistent_task')
      ->condition('tid', $tid)
      ->execute();
  }

  /**
   * Get next task to be processed.
   *
   * @return \stdClass|FALSE
   */
  public static function getNext() {
    $query = db_select('persistent_task', 't');
    $query->fields('t');
    $query->condition('status', array(self::STATUS_NEW, self::STATUS_IN_PROGRESS));
    $query->orderBy('tid', 'ASC');
    $query->range(0, 1);

    $task = $query->execute()->fetch();
    if ($task) {
      $task->data = !empty($task->data) ? unserialize($task->data) : [];
    }

    return $task;
  }
}
