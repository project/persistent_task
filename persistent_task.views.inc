<?php
/**
 * @file
 * Views for persistent tasks.
 */

/**
 * Implements hook_views_data().
 */
function persistent_task_views_data() {
  $data['persistent_task']['table']['group'] = t('Persistent tasks');
  $data['persistent_task']['table']['base'] = array(
    'field' => 'tid',
    'title' => t('Persistent task'),
    'help' => t('List of the persistent tasks'),
  );
  $data['persistent_task']['tid'] = array(
    'title' => t('Task ID'),
    'argument' => array(
      'handler' => 'views_handler_argument',
    ),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $date_type_fields = array(
    'created' => t('Created'),
    'started' => t('Started'),
    'completed' => t('Completed'),
  );
  foreach ($date_type_fields as $key => $value) {
    $data['persistent_task'][$key] = array(
      'title' => $value,
      'argument' => array(
        'handler' => 'views_handler_argument_date',
      ),
      'field' => array(
        'handler' => 'views_handler_field_date',
        'click sortable' => TRUE,
      ),
      'sort' => array(
        'handler' => 'views_handler_sort_date',
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_date',
      ),
    );
  }

  $text_type_fields = array(
    'status' => t('Status'),
    'type' => t('Type'),
  );
  foreach ($text_type_fields as $key => $value) {
    $data['persistent_task'][$key] = array(
      'title' => $value,
      'argument' => array(
        'handler' => 'views_handler_argument_string',
      ),
      'field' => array(
        'handler' => 'views_handler_field',
        'click sortable' => TRUE,
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
      ),
      'filter' => array(
        'handler' => ($key == 'status') ? 'persistent_task_views_handler_filter_task_status' : 'views_handler_filter_string',
      ),
    );
  }

  $data['persistent_task']['data'] = array(
    'title' => t('Data'),
    'field' => array(
      'handler' => 'views_handler_field_serialized',
      'click sortable' => TRUE,
    ),
  );

  return $data;
}
